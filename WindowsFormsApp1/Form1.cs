﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void MessageButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("William Thornton");
        }

        private void ClassButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("CST-117");
        }

        private void TeacherButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Aiman Darwiche");
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
